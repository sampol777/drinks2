plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-kapt")

}

android {
    compileSdk = 32

    defaultConfig {
        applicationId = "com.example.drinkssecondapp"
        minSdk = 21
        targetSdk = 32
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        named("release") {
            isMinifyEnabled = false
            setProguardFiles(
                listOf(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
                )
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
    dynamicFeatures += setOf(":shibu_dynamic_feature")
}

dependencies {

    //implementation("androidx.core:core-ktx:1.8.0")
    implementation(projectLibs.core)
    //implementation("androidx.appcompat:appcompat:1.4.2")
    implementation(projectLibs.appCompat)
    //implementation("com.google.android.material:material:1.6.1")
    implementation(projectLibs.material)
    //implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation(projectLibs.constraintLayout)
    //implementation("com.google.android.play:core-ktx:1.8.1")
    implementation(projectLibs.playCore)
    //testImplementation("junit:junit:4.13.2")
    testImplementation(projectLibs.junit)
    //androidTestImplementation("androidx.test.ext:junit:1.1.3")
    testImplementation(projectLibs.testExt)
    //androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
    androidTestImplementation(projectLibs.espresso)

    //Koin
    //val koinVersion = "3.2.0"
    //implementation("io.insert-koin:koin-android:$koinVersion")
    implementation(projectLibs.koin)

    //Coroutines
    //implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.1")
    implementation(projectLibs.coroutines)
    //implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.6.0-alpha01") //ViewModelscope
    //implementation("androidx.fragment:fragment-ktx:1.5.0")

    //http logging interceptor
    //implementation("com.squareup.okhttp3:logging-interceptor:5.0.0-alpha.2")
    implementation(projectLibs.interceptor)

    //navigation
    //val nav_version = "2.5.0"
    //api("androidx.navigation:navigation-fragment-ktx:$nav_version")
    //api("androidx.navigation:navigation-ui-ktx:$nav_version")
    //api ("androidx.navigation:navigation-dynamic-features-fragment:$nav_version")
    api(projectLibs.bundles.navigation)

    //Glide
    //implementation("com.github.bumptech.glide:glide:4.12.0")
    // Glide v4 uses this new annotation processor -- see https://bumptech.github.io/glide/doc/generatedapi.html
    //annotationProcessor("com.github.bumptech.glide:compiler:4.12.0")
    implementation(projectLibs.bundles.glide)

    implementation (project(":dataModule"))



}