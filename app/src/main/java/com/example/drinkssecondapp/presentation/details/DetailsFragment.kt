package com.example.drinkssecondapp.presentation.details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.drinkssecondapp.databinding.FragmentIngredientsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailsFragment : Fragment() {
    lateinit var binding: FragmentIngredientsBinding
    private val viewModel by viewModel<DetailsVM>()
    val args: DetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentIngredientsBinding.inflate(
            inflater,
            container,
            false
        )
        viewModel.getDetails(args.drinkId)
        initViews()
        initObservers()
        return binding.root
    }

    fun initViews() {
        Log.e("TAG", "initViews: $args")
        binding.name.text = args.drinkName
    }

    fun initObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.detailsState.collect { state ->
                binding.foreverSpinner.isVisible = state.isLoading
                displayingDrink(state)
            }
        }
    }

    fun displayingDrink(state: DetailsVM.DetailsState) {
        with(binding) {
            state.items?.drinks?.firstOrNull()?.let {
                Glide.with(binding.root.context)
                    .load(it.strDrinkThumb)
                    .into(binding.image);
                val s = "${it.strInstructions} ${
                    it.toIngredientsList()
                        .joinToString(
                            prefix = "\n\nIngredients: ",
                            separator = ", ",
                            postfix = "."
                        )
                }"
                details.text = s

            }


        }
    }
}