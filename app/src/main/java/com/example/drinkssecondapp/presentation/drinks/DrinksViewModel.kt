package com.example.drinkssecondapp.presentation.drinks

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.datamodule.domain.use_case.get_drinks.GetDrinksUseCase
import com.example.datamodule.remote.response.Drink
import com.example.datamodule.remote.response.Drinks
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class DrinksViewModel(
    private val getDrinksUseCase: GetDrinksUseCase
) : ViewModel() {
    private val _drinkState:MutableStateFlow<DrinkState> = MutableStateFlow(DrinkState())
    val drinkState: StateFlow<DrinkState> = _drinkState

    fun getDrinks(category:String) {


        viewModelScope.launch {
            with(_drinkState) {
                value = _drinkState.value?.copy(isLoading = true)
                getDrinksUseCase(category).collectLatest { result ->
                    value = if (result.isSuccess) {
                        value.copy(isLoading = false, items = result.getOrDefault(listOf()))
                    } else {
                        value.copy(isLoading = false, error = result.exceptionOrNull())
                    }
                }
            }
        }
    }



    data class DrinkState(
        val isLoading: Boolean = false,
        val item: String = "",
        val items: List<Drink> = emptyList(),
        val error: Throwable?= null
    )
}
