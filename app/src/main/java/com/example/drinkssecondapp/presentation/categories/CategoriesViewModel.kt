package com.example.drinkssecondapp.presentation.categories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.datamodule.domain.use_case.get_categories.GetCategoriesUseCase
import com.example.datamodule.local.entities.Category
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class CategoriesViewModel(
    val getCategoriesUseCase: GetCategoriesUseCase
) : ViewModel() {

    private val _catState: MutableStateFlow<CategoryState> = MutableStateFlow(CategoryState())
    val catState: StateFlow<CategoryState> get() = _catState.asStateFlow()


    fun getCategories() {
//        getCategoriesUseCase().onEach { result ->
//            when(result) {
//                is Resource.Success -> {
//                    _catState.value = CategoryState(items = result.data ?: CategoryResponse(emptyList()))
//                }
//                is Resource.Error -> {
//                    _catState.value = CategoryState(error = result.message ?: "An unexpected error occurred.")
//                }
//                is Resource.Loading -> {
//                    _catState.value = CategoryState(isLoading = true)
//                }
//            }
//        }.launchIn(viewModelScope)

        viewModelScope.launch {

            with(_catState) {
                value = value.copy(isLoading = true)

                getCategoriesUseCase().collectLatest { result ->
                    value = if (result.isSuccess) {
                        value.copy(isLoading = false, items = result.getOrDefault(listOf()))
                    } else {
                        value.copy(isLoading = false, error = result.exceptionOrNull())
                    }
                }

            }
        }

    }


    data class CategoryState(
        val isLoading: Boolean = false,
        val item: String = "Categories",
        val items: List<Category> = emptyList(),
        val error: Throwable? = null
    )
}