package com.example.drinkssecondapp.presentation.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.drinkssecondapp.databinding.FragmentCategoriesBinding
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel


class CategoriesFragment : Fragment() {
    private var _binding: FragmentCategoriesBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CategoriesViewModel by viewModel()

    private val categoryAdapter = CategoryAdapter { category ->
        val action =
            CategoriesFragmentDirections.actionCategoriesFragmentToDrinksFragment(category)
        findNavController().navigate(action)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoriesBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()

    }

    fun initViews() {
        with(binding.categoriesList) {
            layoutManager = GridLayoutManager(this@CategoriesFragment.context, 1)
            adapter = categoryAdapter
        }
    }

    fun initObservers() {
//         lifecycleScope.launchWhenStarted{
//             viewModel.catState.collect { state ->
//                 binding.foreverSpinner.isVisible = state.isLoading
//                 categoryAdapter.updateList(state.items.categories)
//
//             }
//         }

        fun <T> Fragment.collectLatestLifecycleFlow(flow: Flow<T>, collect: suspend (T) -> Unit) {
            lifecycleScope.launchWhenStarted {
                flow.collectLatest(collect)
            }
        }

        viewModel.catState.let {
            collectLatestLifecycleFlow(it) { state ->
                binding.foreverSpinner.isVisible = state.isLoading
                categoryAdapter.updateList(state.items)
            }
        }
        viewModel.getCategories()
    }
}