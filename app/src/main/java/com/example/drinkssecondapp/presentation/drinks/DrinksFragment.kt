package com.example.drinkssecondapp.presentation.drinks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.drinkssecondapp.databinding.FragmentDrinksBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class DrinksFragment: Fragment() {
    lateinit var binding: FragmentDrinksBinding
    private val viewModel by viewModel<DrinksViewModel>()
    val args : DrinksFragmentArgs by navArgs()

    val drinksAdapter = DrinksAdapter { drinkName, drinkId ->
        val action =
            DrinksFragmentDirections.actionDrinksFragmentToDetailsPageFragment(drinkName, drinkId)
        findNavController().navigate(action)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDrinksBinding.inflate(inflater,container,false)
        initViews()
        initObservers()
        return binding.root
    }

    fun initViews() {

        with(binding.drinksList){
            layoutManager = GridLayoutManager(this@DrinksFragment.context, 1)
            adapter = drinksAdapter
        }
    }

    fun initObservers() {

        lifecycleScope.launchWhenStarted{
             viewModel.drinkState.collect { state ->
                 binding.foreverSpinner.isVisible = state.isLoading
                 drinksAdapter.updateList(state.items)

             }
         }
        viewModel?.getDrinks(category = args.category)
    }

}