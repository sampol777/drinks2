package com.example.drinkssecondapp.presentation.categories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.datamodule.local.entities.Category
import com.example.drinkssecondapp.databinding.CatItemBinding

class CategoryAdapter(val adapterListener: (category: String) -> Unit) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var list: MutableList<Category> = mutableListOf()

    fun updateList(newList: List<Category>) {
        val oldSize = list.size
        list.clear()
        notifyItemRangeChanged(0, oldSize)
        list.addAll(newList)
        notifyItemRangeChanged(0, newList.size)
    }

    class CategoryViewHolder(
        val binding: CatItemBinding,
        var vhListener: (category: String) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        fun displayDrinks(category: Category) {
            binding.root.setOnClickListener {
                vhListener.invoke(category.strCategory)
            }
            binding.catName.text = category.strCategory

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            CatItemBinding.inflate(LayoutInflater.from(parent.context)),
            adapterListener
        )

    }

    override fun onBindViewHolder(holder: CategoryAdapter.CategoryViewHolder, position: Int) {
        holder.displayDrinks(list[position])
    }

    override fun getItemCount(): Int = list.size


}