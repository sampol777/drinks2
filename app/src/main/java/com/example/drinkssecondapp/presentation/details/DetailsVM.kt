package com.example.drinkssecondapp.presentation.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.datamodule.domain.use_case.get_details.GetDetailsUseCase
import com.example.datamodule.remote.response.Drinks
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class DetailsVM  constructor(
    private val getDetailsUseCase: GetDetailsUseCase
): ViewModel() {

    private val _detailsState: MutableStateFlow<DetailsVM.DetailsState> = MutableStateFlow(
        DetailsVM.DetailsState()
    )
    val detailsState: StateFlow<DetailsVM.DetailsState> get() = _detailsState.asStateFlow()


    fun getDetails(drinkId: Int) {
        viewModelScope.launch {
            with(_detailsState) {
                value = value.copy(isLoading = true)
                val result = getDetailsUseCase.execute(drinkId)
                value = value.copy(isLoading = false, items = result)
            }
        }
    }


    data class DetailsState(
        val isLoading: Boolean = false,
        val items: Drinks? = null
    )

}