package com.example.drinkssecondapp.presentation.di

import com.example.drinkssecondapp.presentation.categories.CategoriesViewModel
import com.example.drinkssecondapp.presentation.details.DetailsVM
import com.example.drinkssecondapp.presentation.drinks.DrinksViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel{CategoriesViewModel(get())}
    viewModel{DrinksViewModel(get())}
    viewModel{DetailsVM(get())}
}