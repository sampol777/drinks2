package com.example.drinkssecondapp.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.drinkssecondapp.R
import com.example.drinkssecondapp.databinding.ActivityMainBinding
import com.example.drinkssecondapp.presentation.categories.CategoriesFragmentDirections
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.play.core.splitinstall.SplitInstallManager
import com.google.android.play.core.splitinstall.SplitInstallManagerFactory
import com.google.android.play.core.splitinstall.SplitInstallRequest



class MainActivity : AppCompatActivity() {
    lateinit var binding:ActivityMainBinding
    lateinit var navController:NavController
    lateinit var splitInstallManager: SplitInstallManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host) as NavHostFragment
        val navController = navHostFragment.navController
        binding.bottomNavBar.setupWithNavController(navController)

        splitInstallManager = SplitInstallManagerFactory.create(this)

        if(!splitInstallManager.installedModules.contains("shibu_dynamic_feature")){
            val splitInstallRequest= SplitInstallRequest.newBuilder()
                .addModule("shibu_dynamic_feature")
                .build()
            splitInstallManager.startInstall(splitInstallRequest)
                .addOnSuccessListener { result ->
                    Log.e("TAG", "SUCCESS!! Feature Module installed")
                }
                .addOnFailureListener { e ->
                    Log.i("TAG", "Error installing feature module: $e")
                }
        } else {
            Log.e("TAG", "The shibe_dynamic_feature is already installed! :)")
        }

//        binding.bottomNavBar.setOnItemSelectedListener { item ->
//            when (item.itemId) {
//                R.id.drinks-> {
//                    Log.e("TAG", "Cocktails button hit!")
//                    true
//                }
//                R.id.shibies -> {
//                    Log.e("TAG", "Animals button hit")
//                    val action =
//                        CategoriesFragmentDirections.actionCategoryFragmentToIncludedGraph()
//                    navController.navigate(action)
//                    true
//                }
//                else -> false
//            }
//        }


    }
    }
