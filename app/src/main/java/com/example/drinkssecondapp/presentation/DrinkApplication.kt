package com.example.drinkssecondapp.presentation

import android.app.Application
import com.example.datamodule.di.dataModule
import com.example.datamodule.domain.di.domainModule
import com.example.drinkssecondapp.presentation.di.presentationModule
import com.google.android.play.core.splitcompat.SplitCompatApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin


class DrinkApplication : SplitCompatApplication() {
    override fun onCreate() {
        super.onCreate()

        startKoin {

            androidLogger()
            androidContext(this@DrinkApplication)
            modules(
                dataModule,
                domainModule,
                presentationModule
            )
        }
    }
}
