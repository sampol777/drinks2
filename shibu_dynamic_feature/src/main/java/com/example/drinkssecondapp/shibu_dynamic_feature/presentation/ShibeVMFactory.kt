package com.example.drinkssecondapp.shibu_dynamic_feature.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.datamodule.repository.ShibeRepo

class ShibeVMFactory(
    private val repo: ShibeRepo
): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create (ModelClass:Class<T>): T {
        return ShibeViewModel(repo) as T
    }
}