package com.example.drinkssecondapp.shibu_dynamic_feature.presentation

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.datamodule.repository.ShibeRepo
import com.example.drinkssecondapp.shibu_dynamic_feature.databinding.FragmentShibiesBinding


class ShibeFragment : Fragment() {
    private lateinit var binding: FragmentShibiesBinding
    private lateinit var vmFactory: ShibeVMFactory
    private val viewModel by viewModels<ShibeViewModel>() { vmFactory }

    private val theAdapter: ShibeAdapter by lazy {
        ShibeAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentShibiesBinding.inflate(inflater, container, false)
        vmFactory = ShibeVMFactory(ShibeRepo(requireContext()))
        initViews()
        initObservers()
        return binding.root
    }

    fun initViews() {
        with(binding.picures) {
            layoutManager = GridLayoutManager(this@ShibeFragment.context, 4)
            adapter = theAdapter
        }

//        binding.back.setOnClickListener{
//            val action = ShibeFragmentDirections.actionFragmentPicturesPageToMainPageFragment()
//            findNavController().navigate(action)
//        }
    }

    fun initObservers() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            Log.d("123", "initObservers: $state")
            binding.foreverSpinner.isVisible = state.isLoading
            theAdapter.updateList(state.items)
        }
        viewModel.getShibes()
    }


}