package com.example.drinkssecondapp.shibu_dynamic_feature.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.datamodule.local.entities.Animal
import com.example.datamodule.repository.ShibeRepo
import kotlinx.coroutines.launch

class ShibeViewModel(val repo: ShibeRepo) : ViewModel() {
    private val _state: MutableLiveData<ShibeState> = MutableLiveData(ShibeState())
    val state: LiveData<ShibeState> get() = _state


    fun getShibes() {
        viewModelScope.launch {
            _state.value = ShibeState(isLoading = true)
            val result = repo.getShibes()
            Log.d("123", "result: $result")
            _state.value = ShibeState(items = result, isLoading = false)
        }
    }


    data class ShibeState(
        val isLoading: Boolean = false,
        val items: List<Animal> = emptyList()
    )
}