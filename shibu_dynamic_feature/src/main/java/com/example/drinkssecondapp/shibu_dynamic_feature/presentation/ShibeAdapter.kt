package com.example.drinkssecondapp.shibu_dynamic_feature.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.datamodule.local.entities.Animal
import com.example.drinkssecondapp.shibu_dynamic_feature.databinding.PictureItemBinding

class ShibeAdapter: RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>(){

    private var list: MutableList<Animal> = mutableListOf()

    fun updateList(newList: List<Animal>) {
        val oldSize = list.size
        list.clear()
        notifyItemRangeChanged(0, oldSize)
        list.addAll(newList)
        notifyItemRangeChanged(0, newList.size)
    }

    class ShibeViewHolder(val binding: PictureItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun displayImage(animal: Animal) {
            //binding.ivDogImage.load(animal.image)
            Glide.with(binding.root.context)
                .load(animal.image)
                .into(binding.ivDogImage);

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShibeViewHolder {
        return ShibeViewHolder(
            PictureItemBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        holder.displayImage(list[position])
    }

    override fun getItemCount(): Int = list.size
}