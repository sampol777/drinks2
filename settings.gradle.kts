pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    defaultLibrariesExtensionName.set("projectLibs")
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.name = "DrinksSecondApp"
include(":app")
include(":dataModule")
include(":shibu_dynamic_feature")
