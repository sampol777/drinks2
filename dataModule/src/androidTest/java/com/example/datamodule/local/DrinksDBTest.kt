package com.example.datamodule.local

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.datamodule.local.entities.Category
import junit.framework.TestCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SmallTest
class DrinksDBTest:TestCase() {
    private lateinit var db: DrinksDB
    private lateinit var dao: DrinkDao

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, DrinksDB::class.java).build()
        dao = db.drinkDao()
    }

    @After
    fun closeDB(){
       db.close()
    }

    @Test
    fun writeAndReadCategory() = runBlocking{
        val category = Category("SuperDrink")
        dao.insertCategories(listOf(category))
        val categories: Flow<List<Category>> = dao.getCategories()
        val firstCategory: Category = categories.first().first()
        assertEquals(category,firstCategory)


    }
}