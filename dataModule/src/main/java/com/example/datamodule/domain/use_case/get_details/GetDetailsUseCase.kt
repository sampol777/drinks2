package com.example.datamodule.domain.use_case.get_details

import com.example.datamodule.repository.DrinksRepo

class GetDetailsUseCase (
    private val repository: DrinksRepo
        ){
//    operator fun invoke(id:Int): Flow<Resource<DrinkDetails>> = flow {
//        try {
//            emit(Resource.Loading())
//            val getDrinkById = repository.getDetails(id)
//            emit(Resource.Success(getDrinkById))
//
//        } catch (e:HttpException){
//            emit(Resource.Error(e.localizedMessage ?: "An unexpected error occurred."))
//        } catch (e: IOException) {
//            emit(Resource.Error("Could not reach server, check your internet connection."))
//        }
//
//    }
  suspend fun execute(id: Int) = repository.getDetails(id)
}