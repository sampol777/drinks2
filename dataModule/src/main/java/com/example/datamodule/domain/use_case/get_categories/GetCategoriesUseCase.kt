package com.example.datamodule.domain.use_case.get_categories

import com.example.datamodule.local.entities.Category
import com.example.datamodule.repository.DrinksRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class GetCategoriesUseCase(
    private val repository: DrinksRepo
) {

    suspend operator fun invoke(): Flow<Result<List<Category>>> = flow {
        try {
            repository.getCategories()
                .catch { ex ->
                    emit(Result.failure(ex))
                }
                .collect { categories ->
                    emit(Result.success(categories))
                }
        } catch (e: HttpException) {
            emit(Result.failure(e)) //e.localizedMessage ?: "An unexpected error occured"
        } catch (e: IOException) {
            emit(Result.failure(e))//"Couldn't reach sever. Check your internet connection"
        } catch (e: Exception) {
            emit(Result.failure(e))
        }
    }

/*
    suspend fun execute() = repository.getCategories()
*/
}