package com.example.datamodule.domain.di

import com.example.datamodule.domain.use_case.get_categories.GetCategoriesUseCase
import com.example.datamodule.domain.use_case.get_details.GetDetailsUseCase
import com.example.datamodule.domain.use_case.get_drinks.GetDrinksUseCase
import org.koin.dsl.module

val domainModule = module{
    single {GetCategoriesUseCase(get())}
    single {GetDrinksUseCase(get())}
    single {GetDetailsUseCase(get())}

}