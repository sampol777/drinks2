package com.example.datamodule.domain.use_case.get_drinks

import com.example.datamodule.remote.response.Drink
import com.example.datamodule.remote.response.Drinks
import com.example.datamodule.repository.DrinksRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class GetDrinksUseCase (
    private val repository: DrinksRepo
) {
    //suspend fun execute(category: String) = repository.getDrinks(category)

    suspend operator fun invoke(category:String): Flow<Result<List<Drink>>> = flow {
        try {
            repository.getDrinks(category)
                .catch {ex ->
                    emit(Result.failure(ex))
                }
                .collect { drinks ->
                    emit(Result.success(drinks))
                }
        } catch (e: HttpException) {
            emit(Result.failure(e)) //e.localizedMessage ?: "An unexpected error occured"
        } catch (e: IOException) {
            emit(Result.failure(e))//"Couldn't reach sever. Check your internet connection"
        } catch (e: Exception) {
            emit(Result.failure(e))
        }
    }
}