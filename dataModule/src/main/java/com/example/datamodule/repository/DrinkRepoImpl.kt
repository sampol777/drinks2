package com.example.datamodule.repository

import com.example.datamodule.local.DrinkDao
import com.example.datamodule.local.entities.Category
import com.example.datamodule.remote.DrinksService
import com.example.datamodule.remote.response.Drink
import com.example.datamodule.remote.response.Drinks
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collect

@OptIn(ExperimentalCoroutinesApi::class)
class DrinkRepoImpl(
    private val drinksService: DrinksService,
    private val drinkDao: DrinkDao
) : DrinksRepo {
    val scope = CoroutineScope(SupervisorJob() + Dispatchers.IO)

    override suspend fun getCategories(): Flow<List<Category>> = callbackFlow {
        drinkDao.getCategories().collect { list ->
            if (list.isEmpty()) {
                val entities = getNetworkDataAndSaveIt()
                send(entities)
            } else {
                send(list)
            }
        }
    }

    private suspend fun getNetworkDataAndSaveIt(): List<Category> {
        val remoteCategories = scope.async { drinksService.getCategories() }
        val entities = remoteCategories.await().drinks.map {
            Category(it.strCategory)
        }
        saveCategories(entities)
        return entities
    }

    fun saveCategories(entities: List<Category>) =
        scope.launch { drinkDao.insertCategories(entities) }


    override suspend fun getDrinks(category: String): Flow<List<Drink>> = callbackFlow {

        val remoteDrinks =  scope.async {drinksService.getDrinks(category)}
        send(remoteDrinks.await().drinks)
    }

    override suspend fun getDetails(id: Int): Drinks = withContext(Dispatchers.IO) {
        return@withContext drinksService.getDetails(id)
    }
}