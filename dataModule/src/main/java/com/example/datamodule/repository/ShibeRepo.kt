package com.example.datamodule.repository

import android.content.Context
import com.example.datamodule.local.DrinksDB
import com.example.datamodule.local.entities.Animal
import com.example.datamodule.local.entities.AnimalType
import com.example.datamodule.remote.RetrofitProvider
import com.example.datamodule.remote.ShibeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ShibeRepo(context: Context) {

    private val shibeService = RetrofitProvider.getAnimalService()
    private val shibeDao = DrinksDB.getInstance(context).animalDao()

    suspend fun getShibes(count: Int = 20) =
        getStuff(AnimalType.SHIBE) { shibeService.getShibes(count) }

    suspend fun getBirds(count: Int = 20) =
        getStuff(AnimalType.BIRD) { shibeService.getBirds(count) }

    suspend fun getCats(count: Int = 20) =
        getStuff(AnimalType.CAT) { shibeService.getCats(count) }

    suspend fun getStuff(animalType: AnimalType, _fun: suspend () -> List<String>): List<Animal> =
        withContext(Dispatchers.IO) {
            val cachedShibes = shibeDao.getAllTypedAnimals(animalType)
            return@withContext cachedShibes.ifEmpty {
                val remoteDoggos: List<String> = _fun.invoke()
                val entities: List<Animal> = remoteDoggos.map {
                    Animal(type = animalType, image = it)
                }
                shibeDao.insertAnimal(*entities.toTypedArray())
                return@ifEmpty entities
            }
        }
}