package com.example.datamodule.repository

import com.example.datamodule.local.entities.Category
import com.example.datamodule.remote.response.Drink
import com.example.datamodule.remote.response.Drinks
import kotlinx.coroutines.flow.Flow

interface DrinksRepo {
    suspend fun getCategories(): Flow<List<Category>>
    suspend fun getDrinks(category:String): Flow<List<Drink>>
    suspend fun getDetails(id:Int): Drinks
}