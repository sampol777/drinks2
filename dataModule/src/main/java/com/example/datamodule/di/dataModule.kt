package com.example.datamodule.di


import com.example.datamodule.local.DrinksDB
import com.example.datamodule.remote.RetrofitProvider
import com.example.datamodule.repository.DrinkRepoImpl
import com.example.datamodule.repository.DrinksRepo
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataModule = module{
    single { DrinksDB.getInstance(androidContext()).drinkDao()}

    single { RetrofitProvider.getDrinkService()}

    single<DrinksRepo> { DrinkRepoImpl(get(),get()) }
}