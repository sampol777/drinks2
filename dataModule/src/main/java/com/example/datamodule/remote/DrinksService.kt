package com.example.datamodule.remote

import com.example.datamodule.remote.response.Drinks
import retrofit2.http.GET
import retrofit2.http.Query

interface DrinksService {
    companion object {
        const val BASE_URL: String = "https://www.thecocktaildb.com"
        private const val ENDPOINT: String = "/api/json/v1/1/"
        private const val CAT_ROUTE = "${ENDPOINT}list.php" //?c=list
        private const val DRINK_ROUTE = "${ENDPOINT}filter.php" //?c=Ordinary_Drink
        private const val DETAIL_ROUTE = "${ENDPOINT}lookup.php" //?i=11007

    }

    @GET(CAT_ROUTE)
    suspend fun getCategories(@Query("c") category: String = "list"): Drinks

    @GET(DRINK_ROUTE)
    suspend fun getDrinks(@Query("c") category: String): Drinks

    @GET(DETAIL_ROUTE)
    suspend fun  getDetails(@Query("i") drinkId: Int): Drinks
}