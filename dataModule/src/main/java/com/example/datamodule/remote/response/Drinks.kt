package com.example.datamodule.remote.response

data class Drinks(
    val drinks: List<Drink>
)