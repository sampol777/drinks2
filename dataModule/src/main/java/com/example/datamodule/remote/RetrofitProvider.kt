package com.example.datamodule.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create


object RetrofitProvider {


    //http interceptor

    private val retrofitObject: Retrofit.Builder = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())


    fun getDrinkService(): DrinksService = retrofitObject
        .baseUrl(DrinksService.BASE_URL)
        .build()
        .create()

    fun getAnimalService(): ShibeService = retrofitObject
        .baseUrl(ShibeService.BASE_URL)
        .build()
        .create()
}

