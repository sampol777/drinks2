package com.example.datamodule.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.datamodule.local.entities.Category
import kotlinx.coroutines.flow.Flow

@Dao
interface DrinkDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategories(categories: List<Category>)

    @Query("SELECT * FROM categories")
    fun getCategories(): Flow<List<Category>>

//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    suspend fun insertDrink(drink: Drink)
//
//    @Query("SELECT * FROM drinks WHERE idDrink = :id")
//    suspend fun getSingleDrink(id: Int): Drink


}