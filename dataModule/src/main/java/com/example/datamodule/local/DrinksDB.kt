package com.example.datamodule.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.datamodule.local.entities.Animal
import com.example.datamodule.local.entities.Category

@Database(entities = [Category::class, Animal::class], version = 1)
abstract class DrinksDB: RoomDatabase() {
    abstract fun drinkDao(): DrinkDao
    abstract fun animalDao():AnimalDao
    companion object {
        private const val DATABASE_NAME = "drinks.db"


        @Volatile
        private var instance: DrinksDB? = null

        fun getInstance(context: Context): DrinksDB {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context): DrinksDB {
            return Room.databaseBuilder(
                context.applicationContext,
                DrinksDB::class.java,
                DATABASE_NAME
            ).build()
        }
    }
}