package com.example.datamodule.local.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Animal(
    @PrimaryKey(autoGenerate = true)
    val id:Int = 0,
    val type: AnimalType,
    val image:String
)
