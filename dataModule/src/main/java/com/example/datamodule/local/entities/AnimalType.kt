package com.example.datamodule.local.entities

enum class AnimalType {
    SHIBE, CAT, BIRD
}